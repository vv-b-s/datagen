# Datagen

This program is used to randomly insert data into the _Short Story_ database for the _Programming in Databases_ course

### System variables
There are five system properties needed for the application to run:
* `server-uri` - the server uri which points to the address of the server (e.g. `localhost:1433`)
* `database` - the name of the database where the Short Story tables are contained (e.g. `TestBase`)
* `username` - the username for database access (e.g. `sa`)
* `password` - the password for database access

> NOTE: Do not include `http://` in your `server-uri` as this is added by the application

> You can see the implementation of the system variables in `src/main/java/com/company/datagen/PersistenceProperties.java`

### Application execution
To run the application you need only the Java Runtime Environment (8+)

In your terminal run command like this: `java '-Dserver-uri=yourdb:PORT' '-Ddatabase=yourDBName' '-Dusername=yourUsername' '-Dpassword=yourPassword' -jar datagen.jar`

> Replace the variables respectively

### Compilation
If you want to compile this application directly from the source code you will need Maven.

To compile the application run: `mvn clean compile assembly:single` in your terminal.