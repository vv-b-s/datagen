package com.company.datagen;

import com.company.datagen.dao.DataAccessObject;
import com.company.datagen.model.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private static final Random RANDOM = new Random();
    private static final int CLIENTS_COUNT = 100;
    private static final int ADDRESSES_COUNT = 200;
    private static final int CREDITS_COUNT = 101;

    private static final String PERSISTENCE_UNIT_NAME = "ShortStory";
    private static DataAccessObject dao;

    private static List<Client> clients;
    private static List<ClientAddress> addresses;
    private static List<Credit> credits;
    private static List<RepaymentSchedule> repaymentSchedules;

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, PersistenceProperties.getProperties());
        dao = new DataAccessObject(emf.createEntityManager());

        removeAllEntities();

        createClients();
        createAddresses();
        createCredits();
        createRepaymentSchedules();


        System.out.print("Database generated. Press enter to exit...");

        try (Scanner s = new Scanner(System.in)) {
            s.nextLine();
        }

        Runtime.getRuntime().exit(0);
    }

    private static void removeAllEntities() {
        dao.removeAll(RepaymentSchedule.class);
        dao.removeAll(Credit.class);
        dao.removeAll(ClientAddress.class);
        dao.removeAll(Client.class);
    }

    private static void createClients() {
        char[] genders = "MF".toCharArray();
        List<Education> eduCodes = dao.findAll(Education.class);
        List<MaterialStatus> statuses = dao.findAll(MaterialStatus.class);


        for (int i = 1; i <= CLIENTS_COUNT; i++) {
            Client client = new Client(
                    i,
                    String.format("Client%d", i),
                    String.format("Clientov%d", i),
                    String.format("Clientis%d", i),
                    "" + Math.abs(RANDOM.nextInt(10)),
                    genders[Math.abs(RANDOM.nextInt(genders.length))],
                    LocalDate.now().minusYears(Math.abs(RANDOM.nextInt(100))),
                    "" + Math.abs(RANDOM.nextInt(10)),
                    LocalDate.now().minusDays(Math.abs(RANDOM.nextInt(1024))),
                    LocalDate.now().plusDays(Math.abs(RANDOM.nextInt(1024))),
                    "MVR SOFIA",
                    Math.abs(RANDOM.nextDouble() * RANDOM.nextInt(100)),
                    LocalDate.now(),
                    "Someone",
                    eduCodes.get(Math.abs(RANDOM.nextInt(eduCodes.size()))),
                    statuses.get(Math.abs(RANDOM.nextInt(statuses.size())))
            );

            dao.storeEntity(client);
        }

        clients = dao.findAll(Client.class);
    }

    private static void createAddresses() {
        char[] addressTypes = "PCK".toCharArray();

        for (int i = 1; i <= ADDRESSES_COUNT; i++) {
            ClientAddress address = new ClientAddress(
                    i,
                    clients.get(Math.abs(RANDOM.nextInt(clients.size()))),
                    "" + Math.abs(RANDOM.nextInt(4)),
                    addressTypes[Math.abs(RANDOM.nextInt(addressTypes.length))],
                    String.format("Town %d", Math.abs(RANDOM.nextInt(ADDRESSES_COUNT))),
                    "N/A"
            );

            dao.storeEntity(address);
        }

        addresses = dao.findAll(ClientAddress.class);
    }

    private static void createCredits() {
        char[] signed = "YN".toCharArray();
        List<Status> statuses = dao.findAll(Status.class);

        for (int i = 1; i < CREDITS_COUNT; i++) {
            Credit credit = new Credit(
                    (long) i,
                    clients.get(Math.abs(RANDOM.nextInt(clients.size()))),
                    signed[Math.abs(RANDOM.nextInt(signed.length))],
                    LocalDate.now().minusDays(Math.abs(RANDOM.nextInt(1000))),
                    LocalDate.now().plusDays(Math.abs(RANDOM.nextInt(1000))),
                    Math.abs(RANDOM.nextInt(10_000) + RANDOM.nextDouble()),
                    Math.abs(RANDOM.nextDouble()),
                    Math.abs(RANDOM.nextDouble() * RANDOM.nextInt(100)),
                    Math.abs(RANDOM.nextDouble() * RANDOM.nextInt(100)),
                    LocalDate.now(),
                    Math.abs(RANDOM.nextInt(50)),
                    Math.abs(RANDOM.nextDouble() * RANDOM.nextInt(100)),
                    LocalDate.now(),
                    "Someone",
                    statuses.get(Math.abs(RANDOM.nextInt(statuses.size())))
            );

            dao.storeEntity(credit);
        }

        credits = dao.findAll(Credit.class);
    }

    private static void createRepaymentSchedules() {
        for (Credit credit : credits) {
            RepaymentScheduleGenerator.genarateRepaymentSchedule(credit, RANDOM.nextBoolean()).forEach(dao::storeEntity);
        }

        repaymentSchedules = dao.findAll(RepaymentSchedule.class);
    }
}
