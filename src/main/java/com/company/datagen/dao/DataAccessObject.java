package com.company.datagen.dao;

import javax.persistence.EntityManager;
import java.util.List;

public class DataAccessObject {
    private static final String SELECT_QUERY_TEMPLATE = "SELECT e FROM %s e";
    private static final String REMOVE_ALL_QUERY = "DELETE FROM %s";

    private EntityManager em;

    public DataAccessObject(EntityManager em) {
        this.em = em;
    }

    public <T> List<T> findAll(Class<T> entityClass) {
        String selectQuery = String.format(SELECT_QUERY_TEMPLATE, entityClass.getSimpleName());

        return em.createQuery(selectQuery).getResultList();
    }

    public <T> T storeEntity(T entity) {
        runInTransaction(() -> em.merge(entity));
        return entity;
    }

    public <T> void removeAll(Class<T> entityClass) {
        String query = String.format(REMOVE_ALL_QUERY, entityClass.getSimpleName());
        runInTransaction(() -> em.createQuery(query).executeUpdate());
    }

    private void runInTransaction(Runnable statements) {
        em.getTransaction().begin();
        statements.run();
        em.getTransaction().commit();
    }

}
