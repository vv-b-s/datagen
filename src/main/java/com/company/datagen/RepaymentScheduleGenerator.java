package com.company.datagen;

import com.company.datagen.model.Credit;
import com.company.datagen.model.RepaymentSchedule;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RepaymentScheduleGenerator {
    public static List<RepaymentSchedule> genarateRepaymentSchedule(Credit credit, boolean paid) {
        double installmentSum = getInstallmentSum(credit.getInterestRPC() / 12, credit.getInstallmentCount(), credit.getSum());
        LocalDate currentDate = credit.getFistMaturity();
        double installmentRestSum = credit.getSum();


        List<RepaymentSchedule> result = new ArrayList<>();
        for (int i = 0; i < credit.getInstallmentCount(); i++) {
            double currentInterest = installmentRestSum * (credit.getInterestRPC() / 12);
            installmentRestSum -= installmentSum;

            result.add(new RepaymentSchedule(i + 1, currentDate, installmentSum, installmentSum - currentInterest, currentInterest, installmentRestSum, LocalDate.now(), "Me", credit, paid ? 'Y' : 'N'));

            currentDate = currentDate.plusMonths(1);
        }

        return result;
    }

    private static double getInstallmentSum(double interest, int installments, double creditSum) {
        return (creditSum * (interest * Math.pow(1 + interest, installments)))
                / (Math.pow(1 + interest, installments) - 1);
    }
}
