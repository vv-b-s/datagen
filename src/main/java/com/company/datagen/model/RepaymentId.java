package com.company.datagen.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class RepaymentId implements Serializable {

    @Column(name = "INSTALLMENT_NO")
    private int installmentNum;

    @ManyToOne
    @JoinColumn(name = "CREDIT_NO")
    private Credit credit;

    public RepaymentId(int installmentNum, Credit credit) {
        this.installmentNum = installmentNum;
        this.credit = credit;
    }

    public RepaymentId() {}

    public int getInstallmentNum() {
        return installmentNum;
    }

    public Credit getCredit() {
        return credit;
    }
}
