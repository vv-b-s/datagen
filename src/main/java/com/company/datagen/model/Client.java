package com.company.datagen.model;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="CLIENT")
public class Client {

    @Id
    @Column(name = "CLIENT_ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Column(name = "CLIENT_NAME", length = 20)
    private String name;

    @NotNull
    @Column(name = "CLIENT_SURNAME",length = 20)
    private String surname;

    @NotNull
    @Column(name = "CLIENT_LASTNAME", length = 20)
    private String lastName;

    @NotNull
    @Column(name = "CLIENT_EGN", length = 10)
    private String egn;

    @Nullable
    @Column(name = "CLIENT_GENDER", length = 1)
    private Character gender;

    @NotNull
    @Column(name = "CLIENT_BIRTHDATE")
    private LocalDate birthDate;

    @NotNull
    @Column(name = "CLIENT_IDCARD_NO ", length = 10)
    private String idCardNom;

    @NotNull
    @Column(name = "CLIENT_IDCARD_ISSUED")
    private LocalDate idCardIssueed;

    @NotNull
    @Column(name = "CLIENT_IDCARD_VALIDTO")
    private LocalDate idCardValidTo;

    @NotNull
    @Column(name = "CLIENT_IDCARD_ISSUER", length = 10)
    private String idCardIssuer;

    @Nullable
    @Column(name = "CLIENT_INCOME")
    private Double income;

    @Nullable
    @Column(name = "MODIFIED_ON")
    private LocalDate modifiedOn;

    @Nullable
    @Column(name = "MODIFIED_BY", length = 50)
    private String  modifiedBy;

    @ManyToOne
    @JoinColumn(name = "EDU_CODE")
    private Education education;

    @ManyToOne
    @JoinColumn(name = "MSTATUS_CODE")
    private MaterialStatus materialStatus;

    public Client() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Client(int id, String name, String surname, String lastName, String egn, Character gender, LocalDate birthDate, String idCardNom, LocalDate idCardIssueed, LocalDate idCardValidTo, String idCardIssuer, Double income, LocalDate modifiedOn, String modifiedBy, Education education, MaterialStatus materialStatus) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.lastName = lastName;
        this.egn = egn;
        this.gender = gender;
        this.birthDate = birthDate;
        this.idCardNom = idCardNom;
        this.idCardIssueed = idCardIssueed;
        this.idCardValidTo = idCardValidTo;
        this.idCardIssuer = idCardIssuer;
        this.income = income;
        this.modifiedOn = modifiedOn;
        this.modifiedBy = modifiedBy;
        this.education = education;
        this.materialStatus = materialStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public MaterialStatus getMaterialStatus() {
        return materialStatus;
    }

    public void setMaterialStatus(MaterialStatus materialStatus) {
        this.materialStatus = materialStatus;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEgn() {
        return egn;
    }

    public void setEgn(String egn) {
        this.egn = egn;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getIdCardNom() {
        return idCardNom;
    }

    public void setIdCardNom(String idCardNom) {
        this.idCardNom = idCardNom;
    }

    public LocalDate getIdCardIssueed() {
        return idCardIssueed;
    }

    public void setIdCardIssueed(LocalDate idCardIssueed) {
        this.idCardIssueed = idCardIssueed;
    }

    public LocalDate getIdCardValidTo() {
        return idCardValidTo;
    }

    public void setIdCardValidTo(LocalDate idCardValidTo) {
        this.idCardValidTo = idCardValidTo;
    }

    public String getIdCardIssuer() {
        return idCardIssuer;
    }

    public void setIdCardIssuer(String idCardIssuer) {
        this.idCardIssuer = idCardIssuer;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public LocalDate getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(LocalDate modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}