package com.company.datagen.model;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "CREDIT")
public class Credit {
    @Id
    @Column(name = "CREDIT_NO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long creditNumber;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "CLIENT_ID")
    private Client client;

    @Nullable
    @Column(name = "CREDIT_SIGNED")
    private Character signed;

    @NotNull
    @Column(name = "CREDIT_BEGIN_DATE")
    private LocalDate beginDate;

    @NotNull
    @Column(name = "CREDIT_END_DATE")
    private LocalDate endDate;

    @NotNull
    @Column(name = "CREDIT_SUM")
    private double sum;

    @NotNull
    @Column(name = "CREDIT_INTEREST_PRC")
    private double interestRPC;

    @NotNull
    @Column(name = "CREDIT_ALLSUM")
    private double allSum;

    @NotNull
    @Column(name = "CREDIT_GPR")
    private double gpr;

    @NotNull
    @Column(name = "CREDIT_FIRST_MATURITY")
    private LocalDate fistMaturity;

    @NotNull
    @Column(name = "CREDIT_ISNTALLMENTS_CNT")
    private int installmentCount;

    @NotNull
    @Column(name = "CREDIT_INSTALLMENT")
    private double installment;

    @Nullable
    @Column(name = "MODIFIED_ON")
    private LocalDate modifiedOn;

    @Nullable
    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @ManyToOne
    @JoinColumn(name = "STATUS_CODE")
    private Status status;

    public Credit() {
    }

    public Credit(Long creditNumber, Client client, Character signed, LocalDate beginDate, LocalDate endDate, double sum, double interestRPC, double allSum, double gpr, LocalDate fistMaturity, int installmentCount, double installment, LocalDate modifiedOn, String modifiedBy, Status status) {
        this.creditNumber = creditNumber;
        this.client = client;
        this.signed = signed;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.sum = sum;
        this.interestRPC = interestRPC;
        this.allSum = allSum;
        this.gpr = gpr;
        this.fistMaturity = fistMaturity;
        this.installmentCount = installmentCount;
        this.installment = installment;
        this.modifiedOn = modifiedOn;
        this.modifiedBy = modifiedBy;
        this.status = status;
    }

    public void setCreditNumber(Long creditNumber) {
        this.creditNumber = creditNumber;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getCreditNumber() {
        return creditNumber;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Character getSigned() {
        return signed;
    }

    public void setSigned(Character signed) {
        this.signed = signed;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public double getInterestRPC() {
        return interestRPC;
    }

    public void setInterestRPC(double interestRPC) {
        this.interestRPC = interestRPC;
    }

    public double getAllSum() {
        return allSum;
    }

    public void setAllSum(double allSum) {
        this.allSum = allSum;
    }

    public double getGpr() {
        return gpr;
    }

    public void setGpr(double gpr) {
        this.gpr = gpr;
    }

    public LocalDate getFistMaturity() {
        return fistMaturity;
    }

    public void setFistMaturity(LocalDate fistMaturity) {
        this.fistMaturity = fistMaturity;
    }

    public int getInstallmentCount() {
        return installmentCount;
    }

    public void setInstallmentCount(int installmentsCNT) {
        this.installmentCount = installmentsCNT;
    }

    public double getInstallment() {
        return installment;
    }

    public void setInstallment(double installment) {
        this.installment = installment;
    }

    public LocalDate getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(LocalDate modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
