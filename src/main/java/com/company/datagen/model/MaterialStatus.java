package com.company.datagen.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MARITAL_STATUS")
public class MaterialStatus {
    @Id
    @Column(name = "MSTATUS_CODE", length = 1)
    private Character code;

    @Column(name = "MSTATUS_NAME", length = 100)
    private String name;

    public MaterialStatus() {

    }

    public MaterialStatus(Character code, String name) {
        this.code = code;
        this.name = name;
    }

    public Character getCode() {
        return code;
    }

    public void setCode(Character code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
