package com.company.datagen.model;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "REPAYMENT_SCHEDULE")
public class RepaymentSchedule implements Serializable {

    @EmbeddedId
    private RepaymentId repaymentId;

    @NotNull
    @Column(name = "INSTALLMENT_DATE")
    private LocalDate indtallmentDate;

    @NotNull
    @Column(name = "INSTALLMENT_SUM")
    private double installmentSum;

    @NotNull
    @Column(name = "INSTALLMENT_PRINCIPLE")
    private double installmentPrinciple;

    @NotNull
    @Column(name = "INSTALLMENT_INTEREST")
    private double installmentInterest;

    @NotNull
    @Column(name = "INSTALLMENT_RESTSUM")
    private double installmentRestSum;

    @Nullable
    @Column(name = "MODIFIED_ON")
    private LocalDate modifiedOn;

    @Nullable
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;

    @Nullable
    @Column(name = "INSTALLMENT_PAID", length = 1)
    private char installmentPaid;

    public RepaymentSchedule() {
    }

    public RepaymentSchedule(int installmentNum, LocalDate indtallmentDate, double installmentSum, double installmentPrinciple, double installmentInterest, double installmentRestSum, LocalDate modifiedOn, String modifiedBy, Credit credit, char installmentPaid) {
        this.indtallmentDate = indtallmentDate;
        this.installmentSum = installmentSum;
        this.installmentPrinciple = installmentPrinciple;
        this.installmentInterest = installmentInterest;
        this.installmentRestSum = installmentRestSum;
        this.modifiedOn = modifiedOn;
        this.modifiedBy = modifiedBy;
        repaymentId = new RepaymentId(installmentNum, credit);
        this.installmentPaid = installmentPaid;
    }

    public char getInstallmentPaid() {
        return installmentPaid;
    }

    public void setInstallmentPaid(char installmentPaid) {
        this.installmentPaid = installmentPaid;
    }

    public Credit getCredit() {
        return repaymentId.getCredit();
    }

    public int getInstallmentNum() {
        return repaymentId.getInstallmentNum();
    }

    public LocalDate getIndtallmentDate() {
        return indtallmentDate;
    }

    public void setIndtallmentDate(LocalDate indtallmentDate) {
        this.indtallmentDate = indtallmentDate;
    }

    public double getInstallmentSum() {
        return installmentSum;
    }

    public void setInstallmentSum(double installmentSum) {
        this.installmentSum = installmentSum;
    }

    public double getInstallmentPrinciple() {
        return installmentPrinciple;
    }

    public void setInstallmentPrinciple(double installmentPrinciple) {
        this.installmentPrinciple = installmentPrinciple;
    }

    public double getInstallmentInterest() {
        return installmentInterest;
    }

    public void setInstallmentInterest(double installmentInterest) {
        this.installmentInterest = installmentInterest;
    }

    public double getInstallmentRestSum() {
        return installmentRestSum;
    }

    public void setInstallmentRestSum(double installmentRessum) {
        this.installmentRestSum = installmentRessum;
    }

    public LocalDate getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(LocalDate modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
