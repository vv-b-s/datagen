package com.company.datagen.model;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STATUS")
public class Status {
    @Id
    @Column(name = "STATUS_CODE")
    private Character statusCode;

    @NotNull
    @Column(name = "STATUS_NAME", length = 100)
    private String name;

    public Status() {
    }

    public Status(Character statusCode, String name) {
        this.statusCode = statusCode;
        this.name = name;
    }

    public Character getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Character statusCode) {
        this.statusCode = statusCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
