package com.company.datagen.model;

import com.sun.istack.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EDUCATION")
public class Education {
    @Id
    @Column(name = "EDU_CODE", length = 1)
    private Character code;

    @Nullable
    @Column(name = "EDU_NAME")
    private String name;

    public Education() {
    }

    public Education(Character code, String name) {
        this.code = code;
        this.name = name;
    }

    public Character getCode() {
        return code;
    }

    public void setCode(Character code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
