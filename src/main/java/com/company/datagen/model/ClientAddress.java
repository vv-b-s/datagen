package com.company.datagen.model;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

import javax.persistence.*;

@Entity
@Table(name = "CLIENT_ADDRESS")
public class ClientAddress {
    @Id
    @Column(name = "ADDRES_ID")
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "CLIENT_ID")
    private Client client;

    @Nullable
    @Column(name = "ADDRESS_PCODE", length = 5)
    private String postCode;

    @Nullable
    @Column(name = "ADDRESS_TYPE", length = 1)
    private Character addressType;

    @Nullable
    @Column(name = "ADDRESS_TOWN", length = 100)
    private String town;

    @Nullable
    @Column(name = "ADDRESS_TEXT", length = 100)
    private String text;

    public ClientAddress() {
    }

    public ClientAddress(int id, Client client, String postCode, Character addressType, String town, String text) {
        this.id = id;
        this.client = client;
        this.postCode = postCode;
        this.addressType = addressType;
        this.town = town;
        this.text = text;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Character getAddressType() {
        return addressType;
    }

    public void setAddressType(Character addressType) {
        this.addressType = addressType;
    }
}
