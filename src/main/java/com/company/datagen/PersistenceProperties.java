package com.company.datagen;

import java.util.HashMap;
import java.util.Map;

public class PersistenceProperties {
    private static final String[][] PROPERTIES = new String[][]{
            {"javax.persistence.jdbc.url", String.format("jdbc:sqlserver://%s", validateProperty(System.getProperty("server-uri"))) +
                    String.format(";database=%s", validateProperty(System.getProperty("database")))},
            {"javax.persistence.jdbc.user", validateProperty(System.getProperty("username"))},
            {"javax.persistence.jdbc.password", validateProperty(System.getProperty("password"))},
            {"javax.persistence.jdbc.driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver"},
            {"javax.persistence.schema-generation.database.action", "update"},
            {"hibernate.show_sql", "true"},
            {"hibernate.format_sql", "true"},
            {"hibernate.dialect", "org.hibernate.dialect.SQLServerDialect"}
    };

    public static Map<String, String> getProperties() {
        Map<String, String> propertyMap = new HashMap<>();
        for (String[] property : PROPERTIES) {
            propertyMap.put(property[0], property[1]);
        }

        return propertyMap;
    }

    private static String validateProperty(String property) {
        return property == null? "" : property;
    }
}
